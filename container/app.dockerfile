FROM php:7.3-apache-stretch

#installation
RUN apt-get update && apt-get install -y \
    mysql-client \
    zip \
    curl \
    imagemagick \
    libfreetype6-dev \
    libjpeg-turbo-progs \
    libjpeg62-turbo-dev \
    libmcrypt-dev \
    libxml2-dev \
    libcurl4-openssl-dev \
    libicu-dev \
    libpng-dev \
    libjpeg-dev \
    gnupg2 \
    libzip-dev \
    libldap2-dev


RUN docker-php-ext-configure gd --with-freetype-dir=/usr/include/  --with-png-dir=/usr --with-jpeg-dir=/usr\
&& docker-php-ext-configure ldap --with-libdir=lib/x86_64-linux-gnu/ \
&&  docker-php-ext-install \
      curl \
      exif \
      mbstring \
      mysqli \
      opcache \
      pcntl \
      pdo_mysql \
      soap \
      gd \
      intl \
      ldap \
      zip

RUN apt-get clean && apt-get autoremove -q

RUN a2enmod rewrite
#RUN yes | pecl install xdebug \
#    && echo "zend_extension=$(find /usr/local/lib/php/extensions/ -name xdebug.so)" > /usr/local/etc/php/conf.d/xdebug.ini \
#    && echo "xdebug.remote_enable=on" >> /usr/local/etc/php/conf.d/xdebug.ini \
#    && echo "xdebug.remote_autostart=off" >> /usr/local/etc/php/conf.d/xdebug.ini

#config
ADD config/error_high_level_php.ini /usr/local/etc/php/conf.d/conf-custom.ini
RUN a2enmod deflate expires headers mime rewrite \
&& echo "<Directory /var/www/public>\nAllowOverride All\n</Directory>" > /etc/apache2/conf-enabled/allowoverride.conf
ADD config/000-default.conf /etc/apache2/sites-enabled/000-default.conf
ADD config/001-custom.conf /etc/apache2/sites-enabled/001-custom.conf

WORKDIR /var/www/
