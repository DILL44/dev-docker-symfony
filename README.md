# Dev Docker Symfony

docker containers for symfony 4

## Install
install [docker](https://docs.docker.com/install/#supported-platforms) and [docker compose](https://docs.docker.com/compose/install/)   
download this repository `git clone https://gitlab.com/DILL44/dev-docker-symfony.git`   
choose you branch:
* php7.1
* master for php7.3

## Configure
copy .env.dist to .env and edit it :
* `DIRECTORY_PROJECT` is the symfony 4 directory
* `COMPOSE_PROJECT_NAME` is the symfony project name (for different container name)
* `START_IP` is IP range for containers network

## Start
in dev-docker-symfony directory:
```bash
docker-compose up --build -d
docker-compose exec dev_symfony bash
```

## Container
get the container id :
```bash
docker ps
```
get the IP container :
```bash
docker inspect [container-id] | grep '"IPAddress"'
```

### Container list
* `web_symfony` the container with `apache` to test app with IP in you browser
* `dev_symfony` the container for bash with `git`, compooser and frameworks tests (`cs-fixer`, `phpmd`, `phpstan`) ...
* `mysql_symfony` the database container with default database` mysql://root:root@mysql_symfony:3306`
* `phpmyadmin_symfony` the container with `phpmyadmin` to have phpmyadmin with IP in you browser
* `mailcatcher_symfony` the container with `mailcatcher` to catch all mail send by the platform:
 * to configure mailcatcher : `MAILER_URL=smtp://mailcatcher_symfony:1025`
 * to access to mailcatcher whit you browser :` http://[container-IP]:1080`
