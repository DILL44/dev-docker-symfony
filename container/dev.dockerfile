FROM php:7.3-cli-stretch

#installation
RUN apt-get update && apt-get install -y \
    git \
    vim \
    unzip \
    wget \
    mysql-client \
    colordiff \
    zip \
    curl \
    sudo \
    imagemagick \
    locales \
    libcurl4-openssl-dev \
    libfreetype6-dev \
    libjpeg-turbo-progs \
    libjpeg62-turbo-dev \
    libxml2-dev \
    gnupg2 \
    libicu-dev \
    libzip-dev \
    libldap2-dev

RUN docker-php-ext-configure ldap --with-libdir=lib/x86_64-linux-gnu/ \
&& docker-php-ext-install \
    curl \
    exif \
    mbstring \
    mysqli \
    opcache \
    pcntl \
    pdo_mysql \
    soap \
    gd \
    ldap \
    zip


RUN  apt-get install apt-transport-https -y
RUN curl -sS https://dl.yarnpkg.com/debian/pubkey.gpg | sudo apt-key add - \
&& curl -sL https://deb.nodesource.com/setup_12.x | sudo -E bash - \
&& echo "deb https://dl.yarnpkg.com/debian/ stable main" |  tee /etc/apt/sources.list.d/yarn.list \
&& apt-get update &&  apt-get install  nodejs yarn build-essential -y

RUN apt-get clean && apt-get autoremove -q


#install externe (composer)
RUN curl https://getcomposer.org/download/2.1.6/composer.phar -L -o /usr/local/bin/composer \
    && chmod +x /usr/local/bin/composer

#config php
#RUN echo "date.timezone='Europe/Paris'\nmemory_limit = -1\nextension=ast.so\n" > /usr/local/etc/php/conf.d/conf-custom.ini
RUN echo "date.timezone='Europe/Paris'\nmemory_limit = -1\n" > /usr/local/etc/php/conf.d/conf-custom.ini

RUN npm install susy
RUN npm install path

#config lang
RUN sed -i -e "s/# fr_FR.*/fr_FR.UTF-8 UTF-8/" /etc/locale.gen && \
    dpkg-reconfigure --frontend=noninteractive locales
ENV LANG fr_FR.UTF-8

#test
RUN curl -L https://cs.symfony.com/download/php-cs-fixer-v2.phar -o /usr/local/bin/php-cs-fixer \
    && chmod +x /usr/local/bin/php-cs-fixer
RUN curl -L https://github.com/jakzal/phpmd/releases/download/2.6.0-jakzal-3/phpmd.phar  -o /usr/local/bin/phpmd \
    && chmod +x /usr/local/bin/phpmd
RUN curl -L https://github.com/phpstan/phpstan/releases/download/0.11.5/phpstan.phar  -o /usr/local/bin/phpstan \
    && chmod +x /usr/local/bin/phpstan

#RUN pecl install 'xdebug'
#RUN pecl install ast

RUN wget https://get.symfony.com/cli/installer -O - | bash && \
    mv /root/.symfony/bin/symfony /usr/local/bin/symfony

WORKDIR /var/www/
RUN yarn install

ARG UID
ARG GID

RUN groupadd -g ${GID} user
RUN useradd -l -u ${UID} -g user user
ENV PATH=${PATH}:/home/user/bin
RUN echo "user:user" | chpasswd && adduser user sudo
USER user
