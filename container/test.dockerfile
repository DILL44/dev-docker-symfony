FROM php:7.3-apache-stretch

RUN apt-get update && apt-get install -y \
    mysql-client \
    zip \
    curl \
    imagemagick \
    libfreetype6-dev \
    libjpeg-turbo-progs \
    libjpeg62-turbo-dev \
    libxml2-dev \
    libicu-dev \
    libzip-dev \
    git

RUN docker-php-ext-configure gd \
&&  docker-php-ext-install \
      curl \
      exif \
      mbstring \
      mysqli \
      opcache \
      pcntl \
      pdo_mysql \
      soap \
      gd \
      intl \
      zip

RUN apt-get clean && apt-get autoremove -q

#install externe (composer)
RUN curl https://getcomposer.org/download/1.2.0/composer.phar -L -o /usr/local/bin/composer \
    && chmod +x /usr/local/bin/composer
RUN curl -L https://cs.symfony.com/download/php-cs-fixer-v2.phar -o /usr/local/bin/php-cs-fixer \
    && chmod +x /usr/local/bin/php-cs-fixer
RUN curl -L http://static.phpmd.org/php/latest/phpmd.phar  -o /usr/local/bin/phpmd \
    && chmod +x /usr/local/bin/phpmd
RUN curl -L https://github.com/phpstan/phpstan/releases/download/0.11.5/phpstan.phar  -o /usr/local/bin/phpstan \
    && chmod +x /usr/local/bin/phpstan

#config
RUN a2enmod deflate expires headers mime rewrite \
&& echo "<Directory /var/www/public>\nAllowOverride All\n</Directory>" > /etc/apache2/conf-enabled/allowoverride.conf
